import os
import pickle

from devices.KasaDevice import RelayState
from datetime import datetime


class VirtualThreeWay:

    def __init__(self, name, device_ids):
        self.__name = name
        self.__includedDeviceIds = device_ids
        self.__lastStatus = RelayState.OFF
        self.__lastUpdateTime = datetime.utcnow()
        self.__actualDevices = {}
        self.save()

    def device_updated(self, original, updated):
        if updated.mac_address in self.__includedDeviceIds:
            self.__actualDevices[updated.mac_address] = updated
            self.sync_all_switch(updated)

    def device_added(self, new_device):
        if new_device.mac_address in self.__includedDeviceIds:
            self.__actualDevices[new_device.mac_address] = new_device
            self.sync_all_switch(new_device)

    def device_deleted(self, deleted_device):
        if deleted_device.mac_address in self.__includedDeviceIds:
            print("deleted : ", deleted_device.alias)
            del self.__actualDevices[deleted_device.updated.mac_address]

    def sync_all_switch(self, updated_device):
        if updated_device.state is not self.__lastStatus and updated_device.update_time > self.__lastUpdateTime:
            print('Device ', updated_device.alias, ' is now ', updated_device.state)
            for device in self.__actualDevices.values():
                if device.mac_address != updated_device.mac_address:
                    print('Switching ', device.alias, ' to ', updated_device.state)
                    device.state = updated_device.state
            self.__lastStatus = updated_device.state
            self.__lastUpdateTime = datetime.utcnow()

    def save(self):
        filename = './bin/three-way/' + self.__name + '.pkl'
        os.makedirs(os.path.dirname(filename), exist_ok=True)
        with open(filename, 'wb') as output:
            pickle.dump(self, output, pickle.HIGHEST_PROTOCOL)
