#!/usr/bin/python3

import sys
import three_way.virtual_three_way as tw

name = sys.argv[1]
devicesList = sys.argv[2:]

print('Three way name:', str(name))
print('Device List:', str(devicesList))

newThreeWay = tw.VirtualThreeWay(name, devicesList)
