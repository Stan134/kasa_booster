#!/usr/bin/python
import glob
import os
import pickle

import protocol.kasa_device_updater as kdu
import devices.KasaDeviceManager as kdm

os.environ['KIVY_WINDOW'] = 'sdl2'


def load_existing_three_way():
    pickle_files = glob.glob('./bin/three-way/*.pkl')
    for file_name in pickle_files:
        print("Loading ", file_name)
        with open(file_name, 'rb') as file:
            three_way = pickle.load(file)
            manager.device_updated += three_way.device_updated
            manager.device_added += three_way.device_added
            manager.device_deleted += three_way.device_deleted


manager = kdm.KasaDeviceManager()

load_existing_three_way()

updater = kdu.KasaDeviceUpdater(manager)
updater.start()

#time.sleep(60)
#updater.stop()
