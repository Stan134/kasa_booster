

# Encryption and Decryption of TP-Link Smart Home Protocol
# XOR Autokey Cipher with starting key = 171
import struct

INITIALIZATION_VECTOR = 171


def encrypt(request: str) -> bytearray:
    """
    Encrypt a request for a TP-Link Smart Home Device.
    :param request: plaintext request data
    :return: ciphertext request
    """
    key = INITIALIZATION_VECTOR
    buffer = bytearray(struct.pack(">I", len(request)))

    for char in request:
        cipher = key ^ ord(char)
        key = cipher
        buffer.append(cipher)

    return buffer


def decrypt(ciphertext: bytes) -> str:
    """
    Decrypt a response of a TP-Link Smart Home Device.
    :param ciphertext: encrypted response data
    :return: plaintext response
    """
    key = INITIALIZATION_VECTOR
    buffer = []

    ciphertext_str = ciphertext.decode('latin-1')

    for char in ciphertext_str:
        plain = key ^ ord(char)
        key = ord(char)
        buffer.append(chr(plain))

    plaintext = ''.join(buffer)

    return plaintext.encode('latin-1').decode('utf-8')
