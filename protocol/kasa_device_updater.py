import datetime
import json
import threading
import socket
import schedule
import time
import devices.KasaDevice as kd
import protocol.kasa_encryption as ke
import protocol.kasa_commands as kc


class KasaDeviceUpdater(threading.Thread):

    def __init__(self, kasa_device_manager):
        super().__init__()
        self.__kasaDeviceManager = kasa_device_manager
        self.__value = "default"
        self.__job = None
        self.__stopRequested = False

    def run(self):
        self.__job = schedule.every(0.3).seconds.do(self.__query_device_list)

        while not self.__stopRequested:
            schedule.run_pending()
            time.sleep(0.1)

    def __query_device_list(self):
        # print("=================================================")
        # print("Start : ", datetime.datetime.utcnow())
        start_time = datetime.datetime.utcnow()
        sock_udp = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        sock_udp.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
        sock_udp.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        sock_udp.settimeout(0.4)

        req = kc.commands[kc.Command.INFO]
        encrypted_req = ke.encrypt(req)
        sock_udp.sendto(encrypted_req[4:], ("255.255.255.255", kc.PORT))

        try:
            while True:
                data, address = sock_udp.recvfrom(4096)
                info = json.loads(ke.decrypt(data))
                device = kd.KasaDevice(info, address[0], start_time)
                # print(datetime.datetime.utcnow(), ': ', device.alias, ' From ', device.ip_address)
                self.__kasaDeviceManager.add_or_update_device(device)
        except socket.timeout:
            pass

    def stop(self):
        if self.__job is not None:
            schedule.cancel_job(self.__job)

        self.__stopRequested = True
