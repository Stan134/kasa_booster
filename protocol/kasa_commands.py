# Predefined Smart Plug Commands
# For a full list of commands, consult tplink_commands.txt
import json
import socket
from enum import Enum

from protocol.kasa_encryption import encrypt, decrypt


class Command(Enum):
    INFO = 0
    ON = 1
    OFF = 2
    CLOUD_INFO = 3
    WLAN_SCAN = 4
    TIME = 5
    SCHEDULE = 6
    COUNTDOWN = 7
    ANTI_THEFT = 8
    REBOOT = 9
    RESET = 10


PORT = 9999
commands = {Command.INFO: '{"system":{"get_sysinfo":{}}}',
            Command.ON: '{"system":{"set_relay_state":{"state":1}}}',
            Command.OFF: '{"system":{"set_relay_state":{"state":0}}}',
            Command.CLOUD_INFO: '{"cnCloud":{"get_info":{}}}',
            Command.WLAN_SCAN: '{"netif":{"get_scaninfo":{"refresh":0}}}',
            Command.TIME: '{"time":{"get_time":{}}}',
            Command.SCHEDULE: '{"schedule":{"get_rules":{}}}',
            Command.COUNTDOWN: '{"count_down":{"get_rules":{}}}',
            Command.ANTI_THEFT: '{"anti_theft":{"get_rules":{}}}',
            Command.REBOOT: '{"system":{"reboot":{"delay":1}}}',
            Command.RESET: '{"system":{"reset":{"delay":1}}}'
            }


def send_tcp_command(command, ipAddress):
    # Send command and receive reply
    try:
        sock_tcp = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock_tcp.connect((ipAddress, PORT))
        sock_tcp.send(encrypt(commands[command]))
        data = sock_tcp.recv(2048)
        sock_tcp.close()

        payload = json.loads(decrypt(data[4:]))
        system = payload['system']
        command_result = next(iter(system.values()))
        return 0 == command_result['err_code']
    except socket.error:
        return False
