from enum import Enum

from protocol.kasa_commands import send_tcp_command, Command


class RelayState(Enum):
    OFF = 0
    ON = 1


class KasaDevice:

    def __init__(self, dic_system, ip_address, last_update_time):
        dic = dic_system['system']['get_sysinfo']
        self.__errorCode = dic['err_code']
        self.__swVersion = dic['sw_ver']
        self.__hwVersion = dic['hw_ver']
        self.__type = dic['type'] if 'type' in dic else dic['mic_type']
        self.__model = dic['model']
        self.__macAddress = dic['mac']
        self.__deviceId = dic['deviceId']
        self.__hwId = dic['hwId']
        self.__fwId = dic['fwId']
        self.__oemId = dic['oemId']
        self.__alias = dic['alias']
        self.__deviceName = dic['dev_name']
        self.__iconHash = dic['icon_hash']
        self.__relayState = RelayState(dic['relay_state'])
        self.__onTime = dic['on_time']
        self.__activeMode = dic['active_mode']
        self.__feature = dic['feature']
        self.__updating = dic['updating']
        self.__rssi = dic['rssi']
        self.__ledOff = dic['led_off']
        self.__latitude = dic['latitude'] if 'latitude' in dic else (dic['latitude_i'] / 10000)
        self.__longitude = dic['longitude'] if 'longitude' in dic else (dic['longitude_i'] / 10000)
        self.__ipAddress = ip_address
        self.__lastUpdateTime = last_update_time

    @property
    def alias(self):
        return self.__alias

    @property
    def ip_address(self):
        return self.__ipAddress

    @property
    def mac_address(self):
        return self.__macAddress

    @property
    def update_time(self):
        return self.__lastUpdateTime

    @property
    def state(self):
        return self.__relayState

    @state.setter
    def state(self, value):
        if not isinstance(value, RelayState):
            raise TypeError("Not a valid RelayState")

        if send_tcp_command(Command.ON if value is RelayState.ON else Command.OFF, self.__ipAddress):
            self.__relayState = value
        else:
            raise IOError("Can't set the relay state")
