import devices.KasaDevice

class event(object):
    def __init__(self, func):
        self.__doc__ = func.__doc__
        self._key = ' ' + func.__name__

    def __get__(self, obj, cls):
        try:
            return obj.__dict__[self._key]
        except KeyError as exc:
            be = obj.__dict__[self._key] = boundevent()
            return be


class boundevent(object):
    def __init__(self):
        self._fns = []

    def __iadd__(self, fn):
        self._fns.append(fn)
        return self

    def __isub__(self, fn):
        self._fns.remove(fn)
        return self

    def __call__(self, *args, **kwargs):
        for f in self._fns[:]:
            f(*args, **kwargs)


class KasaDeviceManager:

    def __init__(self):
        self.__devices = {}

    @event
    def device_added(self, new_device):
        """Called """

    @event
    def device_updated(self, original_device, new_device):
        """Called """

    @event
    def device_deleted(self, deleted_device):
        """Called """

    def add_or_update_device(self, device: devices.KasaDevice):
        if device.mac_address in self.__devices:
            original_device = self.__devices[device.mac_address]
            self.__devices[device.mac_address] = device
            self.device_updated(original_device, device)
        else:
            self.__devices[device.mac_address] = device
            self.device_added(device)
